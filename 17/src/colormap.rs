use termcolor::Color;

pub struct ColorMap(&'static [Color]);

#[allow(dead_code)]
pub const COLORMAP_SMOOTH_COOLWARM: ColorMap = ColorMap(&[
    Color::Rgb(59, 76, 19),
    Color::Rgb(60, 78, 19),
    Color::Rgb(61, 80, 19),
    Color::Rgb(62, 81, 19),
    Color::Rgb(63, 83, 19),
    Color::Rgb(64, 85, 20),
    Color::Rgb(66, 87, 20),
    Color::Rgb(67, 88, 20),
    Color::Rgb(68, 90, 20),
    Color::Rgb(69, 92, 20),
    Color::Rgb(70, 94, 20),
    Color::Rgb(72, 95, 20),
    Color::Rgb(73, 97, 21),
    Color::Rgb(74, 99, 21),
    Color::Rgb(75, 100, 21),
    Color::Rgb(76, 102, 21),
    Color::Rgb(78, 104, 21),
    Color::Rgb(79, 106, 21),
    Color::Rgb(80, 107, 21),
    Color::Rgb(81, 109, 21),
    Color::Rgb(83, 111, 22),
    Color::Rgb(84, 112, 22),
    Color::Rgb(85, 114, 22),
    Color::Rgb(86, 116, 22),
    Color::Rgb(88, 117, 22),
    Color::Rgb(89, 119, 22),
    Color::Rgb(90, 120, 22),
    Color::Rgb(91, 122, 22),
    Color::Rgb(93, 124, 23),
    Color::Rgb(94, 125, 23),
    Color::Rgb(95, 127, 23),
    Color::Rgb(97, 129, 23),
    Color::Rgb(98, 130, 23),
    Color::Rgb(99, 132, 23),
    Color::Rgb(101, 133, 23),
    Color::Rgb(102, 135, 23),
    Color::Rgb(103, 136, 23),
    Color::Rgb(105, 138, 23),
    Color::Rgb(106, 140, 24),
    Color::Rgb(107, 141, 24),
    Color::Rgb(109, 143, 24),
    Color::Rgb(110, 144, 24),
    Color::Rgb(111, 146, 24),
    Color::Rgb(113, 147, 24),
    Color::Rgb(114, 149, 24),
    Color::Rgb(115, 150, 24),
    Color::Rgb(117, 152, 24),
    Color::Rgb(118, 153, 24),
    Color::Rgb(119, 154, 24),
    Color::Rgb(121, 156, 24),
    Color::Rgb(122, 157, 24),
    Color::Rgb(123, 159, 24),
    Color::Rgb(125, 160, 24),
    Color::Rgb(126, 162, 25),
    Color::Rgb(128, 163, 25),
    Color::Rgb(129, 164, 25),
    Color::Rgb(130, 166, 25),
    Color::Rgb(132, 167, 25),
    Color::Rgb(133, 168, 25),
    Color::Rgb(134, 170, 25),
    Color::Rgb(136, 171, 25),
    Color::Rgb(137, 172, 25),
    Color::Rgb(139, 174, 25),
    Color::Rgb(140, 175, 25),
    Color::Rgb(141, 176, 25),
    Color::Rgb(143, 177, 25),
    Color::Rgb(144, 178, 25),
    Color::Rgb(146, 180, 25),
    Color::Rgb(147, 181, 25),
    Color::Rgb(148, 182, 25),
    Color::Rgb(150, 183, 25),
    Color::Rgb(151, 184, 25),
    Color::Rgb(153, 186, 25),
    Color::Rgb(154, 187, 25),
    Color::Rgb(155, 188, 25),
    Color::Rgb(157, 189, 25),
    Color::Rgb(158, 190, 25),
    Color::Rgb(159, 191, 25),
    Color::Rgb(161, 192, 25),
    Color::Rgb(162, 193, 25),
    Color::Rgb(163, 194, 25),
    Color::Rgb(165, 195, 25),
    Color::Rgb(166, 196, 25),
    Color::Rgb(168, 197, 25),
    Color::Rgb(169, 198, 25),
    Color::Rgb(170, 199, 25),
    Color::Rgb(172, 200, 25),
    Color::Rgb(173, 201, 25),
    Color::Rgb(174, 201, 25),
    Color::Rgb(176, 202, 25),
    Color::Rgb(177, 203, 25),
    Color::Rgb(178, 204, 25),
    Color::Rgb(180, 205, 25),
    Color::Rgb(181, 206, 25),
    Color::Rgb(182, 206, 25),
    Color::Rgb(183, 207, 24),
    Color::Rgb(185, 208, 24),
    Color::Rgb(186, 209, 24),
    Color::Rgb(187, 209, 24),
    Color::Rgb(189, 210, 24),
    Color::Rgb(190, 211, 24),
    Color::Rgb(191, 211, 24),
    Color::Rgb(192, 212, 24),
    Color::Rgb(193, 212, 24),
    Color::Rgb(195, 213, 24),
    Color::Rgb(196, 214, 24),
    Color::Rgb(197, 214, 24),
    Color::Rgb(198, 215, 24),
    Color::Rgb(200, 215, 24),
    Color::Rgb(201, 216, 24),
    Color::Rgb(202, 216, 23),
    Color::Rgb(203, 216, 23),
    Color::Rgb(204, 217, 23),
    Color::Rgb(205, 217, 23),
    Color::Rgb(206, 218, 23),
    Color::Rgb(208, 218, 23),
    Color::Rgb(209, 218, 23),
    Color::Rgb(210, 219, 23),
    Color::Rgb(211, 219, 23),
    Color::Rgb(212, 219, 23),
    Color::Rgb(213, 219, 22),
    Color::Rgb(214, 220, 22),
    Color::Rgb(215, 220, 22),
    Color::Rgb(216, 220, 22),
    Color::Rgb(217, 220, 22),
    Color::Rgb(218, 220, 22),
    Color::Rgb(219, 221, 22),
    Color::Rgb(220, 221, 22),
    Color::Rgb(221, 220, 22),
    Color::Rgb(222, 220, 21),
    Color::Rgb(223, 220, 21),
    Color::Rgb(225, 219, 21),
    Color::Rgb(226, 218, 21),
    Color::Rgb(227, 218, 21),
    Color::Rgb(228, 217, 21),
    Color::Rgb(229, 217, 21),
    Color::Rgb(229, 216, 20),
    Color::Rgb(230, 216, 20),
    Color::Rgb(231, 215, 20),
    Color::Rgb(232, 214, 20),
    Color::Rgb(233, 214, 20),
    Color::Rgb(234, 213, 20),
    Color::Rgb(235, 212, 20),
    Color::Rgb(235, 211, 19),
    Color::Rgb(236, 211, 19),
    Color::Rgb(237, 210, 19),
    Color::Rgb(238, 209, 19),
    Color::Rgb(238, 208, 19),
    Color::Rgb(239, 207, 19),
    Color::Rgb(239, 206, 18),
    Color::Rgb(240, 206, 18),
    Color::Rgb(241, 205, 18),
    Color::Rgb(241, 204, 18),
    Color::Rgb(242, 203, 18),
    Color::Rgb(242, 202, 18),
    Color::Rgb(243, 201, 18),
    Color::Rgb(243, 200, 17),
    Color::Rgb(244, 199, 17),
    Color::Rgb(244, 198, 17),
    Color::Rgb(244, 197, 17),
    Color::Rgb(245, 196, 17),
    Color::Rgb(245, 195, 17),
    Color::Rgb(245, 193, 16),
    Color::Rgb(246, 192, 16),
    Color::Rgb(246, 191, 16),
    Color::Rgb(246, 190, 16),
    Color::Rgb(246, 189, 16),
    Color::Rgb(247, 188, 16),
    Color::Rgb(247, 186, 15),
    Color::Rgb(247, 185, 15),
    Color::Rgb(247, 184, 15),
    Color::Rgb(247, 183, 15),
    Color::Rgb(247, 181, 15),
    Color::Rgb(247, 180, 15),
    Color::Rgb(247, 179, 15),
    Color::Rgb(247, 177, 14),
    Color::Rgb(247, 176, 14),
    Color::Rgb(247, 175, 14),
    Color::Rgb(247, 173, 14),
    Color::Rgb(247, 172, 14),
    Color::Rgb(247, 171, 14),
    Color::Rgb(247, 169, 13),
    Color::Rgb(247, 168, 13),
    Color::Rgb(247, 166, 13),
    Color::Rgb(246, 165, 13),
    Color::Rgb(246, 163, 13),
    Color::Rgb(246, 162, 13),
    Color::Rgb(246, 160, 12),
    Color::Rgb(245, 159, 12),
    Color::Rgb(245, 157, 12),
    Color::Rgb(245, 156, 12),
    Color::Rgb(244, 154, 12),
    Color::Rgb(244, 153, 12),
    Color::Rgb(244, 151, 12),
    Color::Rgb(243, 149, 11),
    Color::Rgb(243, 148, 11),
    Color::Rgb(242, 146, 11),
    Color::Rgb(242, 144, 11),
    Color::Rgb(241, 143, 11),
    Color::Rgb(241, 141, 11),
    Color::Rgb(240, 139, 11),
    Color::Rgb(240, 138, 10),
    Color::Rgb(239, 136, 10),
    Color::Rgb(238, 134, 10),
    Color::Rgb(238, 133, 10),
    Color::Rgb(237, 131, 10),
    Color::Rgb(237, 129, 10),
    Color::Rgb(236, 127, 99),
    Color::Rgb(235, 125, 98),
    Color::Rgb(234, 124, 96),
    Color::Rgb(234, 122, 95),
    Color::Rgb(233, 120, 93),
    Color::Rgb(232, 118, 92),
    Color::Rgb(231, 116, 91),
    Color::Rgb(230, 114, 89),
    Color::Rgb(229, 112, 88),
    Color::Rgb(229, 111, 86),
    Color::Rgb(228, 109, 85),
    Color::Rgb(227, 107, 84),
    Color::Rgb(226, 105, 82),
    Color::Rgb(225, 103, 81),
    Color::Rgb(224, 101, 79),
    Color::Rgb(223, 99, 78),
    Color::Rgb(222, 97, 77),
    Color::Rgb(221, 95, 75),
    Color::Rgb(220, 93, 74),
    Color::Rgb(219, 91, 73),
    Color::Rgb(218, 89, 71),
    Color::Rgb(216, 86, 70),
    Color::Rgb(215, 84, 69),
    Color::Rgb(214, 82, 68),
    Color::Rgb(213, 80, 66),
    Color::Rgb(212, 78, 65),
    Color::Rgb(211, 76, 64),
    Color::Rgb(209, 73, 62),
    Color::Rgb(208, 71, 61),
    Color::Rgb(207, 69, 60),
    Color::Rgb(206, 67, 59),
    Color::Rgb(204, 64, 57),
    Color::Rgb(203, 62, 56),
    Color::Rgb(202, 59, 55),
    Color::Rgb(200, 57, 54),
    Color::Rgb(199, 54, 53),
    Color::Rgb(198, 52, 52),
    Color::Rgb(196, 49, 50),
    Color::Rgb(195, 46, 49),
    Color::Rgb(193, 43, 48),
    Color::Rgb(192, 40, 47),
    Color::Rgb(191, 37, 46),
    Color::Rgb(189, 34, 45),
    Color::Rgb(188, 30, 44),
    Color::Rgb(186, 26, 43),
    Color::Rgb(185, 22, 41),
    Color::Rgb(183, 17, 40),
    Color::Rgb(182, 11, 39),
    Color::Rgb(180, 4, 38),
]);

#[allow(dead_code)]
pub const COLORMAP_INFERNO: ColorMap = ColorMap(&[
    Color::Rgb(0, 0, 4),
    Color::Rgb(1, 0, 5),
    Color::Rgb(1, 1, 6),
    Color::Rgb(1, 1, 8),
    Color::Rgb(2, 1, 10),
    Color::Rgb(2, 2, 12),
    Color::Rgb(2, 2, 14),
    Color::Rgb(3, 2, 16),
    Color::Rgb(4, 3, 18),
    Color::Rgb(4, 3, 20),
    Color::Rgb(5, 4, 23),
    Color::Rgb(6, 4, 25),
    Color::Rgb(7, 5, 27),
    Color::Rgb(8, 5, 29),
    Color::Rgb(9, 6, 31),
    Color::Rgb(10, 7, 34),
    Color::Rgb(11, 7, 36),
    Color::Rgb(12, 8, 38),
    Color::Rgb(13, 8, 41),
    Color::Rgb(14, 9, 43),
    Color::Rgb(16, 9, 45),
    Color::Rgb(17, 10, 48),
    Color::Rgb(18, 10, 50),
    Color::Rgb(20, 11, 52),
    Color::Rgb(21, 11, 55),
    Color::Rgb(22, 11, 57),
    Color::Rgb(24, 12, 60),
    Color::Rgb(25, 12, 62),
    Color::Rgb(27, 12, 65),
    Color::Rgb(28, 12, 67),
    Color::Rgb(30, 12, 69),
    Color::Rgb(31, 12, 72),
    Color::Rgb(33, 12, 74),
    Color::Rgb(35, 12, 76),
    Color::Rgb(36, 12, 79),
    Color::Rgb(38, 12, 81),
    Color::Rgb(40, 11, 83),
    Color::Rgb(41, 11, 85),
    Color::Rgb(43, 11, 87),
    Color::Rgb(45, 11, 89),
    Color::Rgb(47, 10, 91),
    Color::Rgb(49, 10, 92),
    Color::Rgb(50, 10, 94),
    Color::Rgb(52, 10, 95),
    Color::Rgb(54, 9, 97),
    Color::Rgb(56, 9, 98),
    Color::Rgb(57, 9, 99),
    Color::Rgb(59, 9, 100),
    Color::Rgb(61, 9, 101),
    Color::Rgb(62, 9, 102),
    Color::Rgb(64, 10, 103),
    Color::Rgb(66, 10, 104),
    Color::Rgb(68, 10, 104),
    Color::Rgb(69, 10, 105),
    Color::Rgb(71, 11, 106),
    Color::Rgb(73, 11, 106),
    Color::Rgb(74, 12, 107),
    Color::Rgb(76, 12, 107),
    Color::Rgb(77, 13, 108),
    Color::Rgb(79, 13, 108),
    Color::Rgb(81, 14, 108),
    Color::Rgb(82, 14, 109),
    Color::Rgb(84, 15, 109),
    Color::Rgb(85, 15, 109),
    Color::Rgb(87, 16, 110),
    Color::Rgb(89, 16, 110),
    Color::Rgb(90, 17, 110),
    Color::Rgb(92, 18, 110),
    Color::Rgb(93, 18, 110),
    Color::Rgb(95, 19, 110),
    Color::Rgb(97, 19, 110),
    Color::Rgb(98, 20, 110),
    Color::Rgb(100, 21, 110),
    Color::Rgb(101, 21, 110),
    Color::Rgb(103, 22, 110),
    Color::Rgb(105, 22, 110),
    Color::Rgb(106, 23, 110),
    Color::Rgb(108, 24, 110),
    Color::Rgb(109, 24, 110),
    Color::Rgb(111, 25, 110),
    Color::Rgb(113, 25, 110),
    Color::Rgb(114, 26, 110),
    Color::Rgb(116, 26, 110),
    Color::Rgb(117, 27, 110),
    Color::Rgb(119, 28, 109),
    Color::Rgb(120, 28, 109),
    Color::Rgb(122, 29, 109),
    Color::Rgb(124, 29, 109),
    Color::Rgb(125, 30, 109),
    Color::Rgb(127, 30, 108),
    Color::Rgb(128, 31, 108),
    Color::Rgb(130, 32, 108),
    Color::Rgb(132, 32, 107),
    Color::Rgb(133, 33, 107),
    Color::Rgb(135, 33, 107),
    Color::Rgb(136, 34, 106),
    Color::Rgb(138, 34, 106),
    Color::Rgb(140, 35, 105),
    Color::Rgb(141, 35, 105),
    Color::Rgb(143, 36, 105),
    Color::Rgb(144, 37, 104),
    Color::Rgb(146, 37, 104),
    Color::Rgb(147, 38, 103),
    Color::Rgb(149, 38, 103),
    Color::Rgb(151, 39, 102),
    Color::Rgb(152, 39, 102),
    Color::Rgb(154, 40, 101),
    Color::Rgb(155, 41, 100),
    Color::Rgb(157, 41, 100),
    Color::Rgb(159, 42, 99),
    Color::Rgb(160, 42, 99),
    Color::Rgb(162, 43, 98),
    Color::Rgb(163, 44, 97),
    Color::Rgb(165, 44, 96),
    Color::Rgb(166, 45, 96),
    Color::Rgb(168, 46, 95),
    Color::Rgb(169, 46, 94),
    Color::Rgb(171, 47, 94),
    Color::Rgb(173, 48, 93),
    Color::Rgb(174, 48, 92),
    Color::Rgb(176, 49, 91),
    Color::Rgb(177, 50, 90),
    Color::Rgb(179, 50, 90),
    Color::Rgb(180, 51, 89),
    Color::Rgb(182, 52, 88),
    Color::Rgb(183, 53, 87),
    Color::Rgb(185, 53, 86),
    Color::Rgb(186, 54, 85),
    Color::Rgb(188, 55, 84),
    Color::Rgb(189, 56, 83),
    Color::Rgb(191, 57, 82),
    Color::Rgb(192, 58, 81),
    Color::Rgb(193, 58, 80),
    Color::Rgb(195, 59, 79),
    Color::Rgb(196, 60, 78),
    Color::Rgb(198, 61, 77),
    Color::Rgb(199, 62, 76),
    Color::Rgb(200, 63, 75),
    Color::Rgb(202, 64, 74),
    Color::Rgb(203, 65, 73),
    Color::Rgb(204, 66, 72),
    Color::Rgb(206, 67, 71),
    Color::Rgb(207, 68, 70),
    Color::Rgb(208, 69, 69),
    Color::Rgb(210, 70, 68),
    Color::Rgb(211, 71, 67),
    Color::Rgb(212, 72, 66),
    Color::Rgb(213, 74, 65),
    Color::Rgb(215, 75, 63),
    Color::Rgb(216, 76, 62),
    Color::Rgb(217, 77, 61),
    Color::Rgb(218, 78, 60),
    Color::Rgb(219, 80, 59),
    Color::Rgb(221, 81, 58),
    Color::Rgb(222, 82, 56),
    Color::Rgb(223, 83, 55),
    Color::Rgb(224, 85, 54),
    Color::Rgb(225, 86, 53),
    Color::Rgb(226, 87, 52),
    Color::Rgb(227, 89, 51),
    Color::Rgb(228, 90, 49),
    Color::Rgb(229, 92, 48),
    Color::Rgb(230, 93, 47),
    Color::Rgb(231, 94, 46),
    Color::Rgb(232, 96, 45),
    Color::Rgb(233, 97, 43),
    Color::Rgb(234, 99, 42),
    Color::Rgb(235, 100, 41),
    Color::Rgb(235, 102, 40),
    Color::Rgb(236, 103, 38),
    Color::Rgb(237, 105, 37),
    Color::Rgb(238, 106, 36),
    Color::Rgb(239, 108, 35),
    Color::Rgb(239, 110, 33),
    Color::Rgb(240, 111, 32),
    Color::Rgb(241, 113, 31),
    Color::Rgb(241, 115, 29),
    Color::Rgb(242, 116, 28),
    Color::Rgb(243, 118, 27),
    Color::Rgb(243, 120, 25),
    Color::Rgb(244, 121, 24),
    Color::Rgb(245, 123, 23),
    Color::Rgb(245, 125, 21),
    Color::Rgb(246, 126, 20),
    Color::Rgb(246, 128, 19),
    Color::Rgb(247, 130, 18),
    Color::Rgb(247, 132, 16),
    Color::Rgb(248, 133, 15),
    Color::Rgb(248, 135, 14),
    Color::Rgb(248, 137, 12),
    Color::Rgb(249, 139, 11),
    Color::Rgb(249, 140, 10),
    Color::Rgb(249, 142, 9),
    Color::Rgb(250, 144, 8),
    Color::Rgb(250, 146, 7),
    Color::Rgb(250, 148, 7),
    Color::Rgb(251, 150, 6),
    Color::Rgb(251, 151, 6),
    Color::Rgb(251, 153, 6),
    Color::Rgb(251, 155, 6),
    Color::Rgb(251, 157, 7),
    Color::Rgb(252, 159, 7),
    Color::Rgb(252, 161, 8),
    Color::Rgb(252, 163, 9),
    Color::Rgb(252, 165, 10),
    Color::Rgb(252, 166, 12),
    Color::Rgb(252, 168, 13),
    Color::Rgb(252, 170, 15),
    Color::Rgb(252, 172, 17),
    Color::Rgb(252, 174, 18),
    Color::Rgb(252, 176, 20),
    Color::Rgb(252, 178, 22),
    Color::Rgb(252, 180, 24),
    Color::Rgb(251, 182, 26),
    Color::Rgb(251, 184, 29),
    Color::Rgb(251, 186, 31),
    Color::Rgb(251, 188, 33),
    Color::Rgb(251, 190, 35),
    Color::Rgb(250, 192, 38),
    Color::Rgb(250, 194, 40),
    Color::Rgb(250, 196, 42),
    Color::Rgb(250, 198, 45),
    Color::Rgb(249, 199, 47),
    Color::Rgb(249, 201, 50),
    Color::Rgb(249, 203, 53),
    Color::Rgb(248, 205, 55),
    Color::Rgb(248, 207, 58),
    Color::Rgb(247, 209, 61),
    Color::Rgb(247, 211, 64),
    Color::Rgb(246, 213, 67),
    Color::Rgb(246, 215, 70),
    Color::Rgb(245, 217, 73),
    Color::Rgb(245, 219, 76),
    Color::Rgb(244, 221, 79),
    Color::Rgb(244, 223, 83),
    Color::Rgb(244, 225, 86),
    Color::Rgb(243, 227, 90),
    Color::Rgb(243, 229, 93),
    Color::Rgb(242, 230, 97),
    Color::Rgb(242, 232, 101),
    Color::Rgb(242, 234, 105),
    Color::Rgb(241, 236, 109),
    Color::Rgb(241, 237, 113),
    Color::Rgb(241, 239, 117),
    Color::Rgb(241, 241, 121),
    Color::Rgb(242, 242, 125),
    Color::Rgb(242, 244, 130),
    Color::Rgb(243, 245, 134),
    Color::Rgb(243, 246, 138),
    Color::Rgb(244, 248, 142),
    Color::Rgb(245, 249, 146),
    Color::Rgb(246, 250, 150),
    Color::Rgb(248, 251, 154),
    Color::Rgb(249, 252, 157),
    Color::Rgb(250, 253, 161),
    Color::Rgb(252, 255, 164),
]);

impl ColorMap {
    pub fn apply(&self, val: f64) -> Color {
        assert!(val >= 0.0 && val <= 1.0);
        let position = (val * 255.0).ceil() as usize;
        self.0[position]
    }
}
