use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::ops::Range;
use std::time::Instant;

use itertools::Itertools;

// --- Day 5: If You Give A Seed A Fertilizer ---

// You take the boat and find the gardener right where you were told he would be:
// managing a giant "garden" that looks more to you like a farm.

// "A water source? Island Island is the water source!" You point out that Snow Island
// isn't receiving any water.

// "Oh, we had to stop the water because we ran out of sand to filter it with! Can't
// make snow with dirty water. Don't worry, I'm sure we'll get more sand soon; we only
// turned off the water a few days... weeks... oh no." His face sinks into a look of
// horrified realization.

// "I've been so busy making sure everyone here has food that I completely forgot to
// check why we stopped getting more sand! There's a ferry leaving soon that is headed
// over in that direction - it's much faster than your boat. Could you please go check
// it out?"

// You barely have time to agree to this request when he brings up another. "While you
// wait for the ferry, maybe you can help us with our food production problem. The
// latest Island Island Almanac just arrived and we're having trouble making sense of
// it."

// The almanac (your puzzle input) lists all of the seeds that need to be planted. It
// also lists what type of soil to use with each kind of seed, what type of fertilizer
// to use with each kind of soil, what type of water to use with each kind of
// fertilizer, and so on. Every type of seed, soil, fertilizer and so on is identified
// with a number, but numbers are reused by each category - that is, soil 123 and
// fertilizer 123 aren't necessarily related to each other.

// For example:

// seeds: 79 14 55 13
//
// seed-to-soil map:
// 50 98 2
// 52 50 48
//
// soil-to-fertilizer map:
// 0 15 37
// 37 52 2
// 39 0 15
//
// fertilizer-to-water map:
// 49 53 8
// 0 11 42
// 42 0 7
// 57 7 4
//
// water-to-light map:
// 88 18 7
// 18 25 70
//
// light-to-temperature map:
// 45 77 23
// 81 45 19
// 68 64 13
//
// temperature-to-humidity map:
// 0 69 1
// 1 0 69
//
// humidity-to-location map:
// 60 56 37
// 56 93 4

// The almanac starts by listing which seeds need to be planted: seeds 79, 14, 55, and 13.

// The rest of the almanac contains a list of maps which describe how to convert numbers
// from a source category into numbers in a destination category. That is, the section
// that starts with seed-to-soil map: describes how to convert a seed number (the
// source) to a soil number (the destination). This lets the gardener and his team know
// which soil to use with which seeds, which water to use with which fertilizer, and so
// on.

// Rather than list every source number and its corresponding destination number one by
// one, the maps describe entire ranges of numbers that can be converted. Each line
// within a map contains three numbers: the destination range start, the source range
// start, and the range length.

// Consider again the example seed-to-soil map:

// 50 98 2
// 52 50 48

// The first line has a destination range start of 50, a source range start of 98, and a
// range length of 2. This line means that the source range starts at 98 and contains
// two values: 98 and 99. The destination range is the same length, but it starts at 50,
// so its two values are 50 and 51. With this information, you know that seed number 98
// corresponds to soil number 50 and that seed number 99 corresponds to soil number 51.

// The second line means that the source range starts at 50 and contains 48 values: 50,
// 51, ..., 96, 97. This corresponds to a destination range starting at 52 and also
// containing 48 values: 52, 53, ..., 98, 99. So, seed number 53 corresponds to soil
// number 55.

// Any source numbers that aren't mapped correspond to the same destination number. So,
// seed number 10 corresponds to soil number 10.

// So, the entire list of seed numbers and their corresponding soil numbers looks like
// this:

// seed  soil
// 0     0
// 1     1
// ...   ...
// 48    48
// 49    49
// 50    52
// 51    53
// ...   ...
// 96    98
// 97    99
// 98    50
// 99    51

// With this map, you can look up the soil number required for each initial seed number:

//     Seed number 79 corresponds to soil number 81.
//     Seed number 14 corresponds to soil number 14.
//     Seed number 55 corresponds to soil number 57.
//     Seed number 13 corresponds to soil number 13.

// BOILERPLATE
type InputIter = Lines<BufReader<File>>;

fn get_input() -> InputIter {
    let f = File::open("input").unwrap();
    let br = BufReader::new(f);
    br.lines()
}

fn main() {
    let start = Instant::now();
    let ans1 = problem1(get_input());
    let duration = start.elapsed();
    println!("Problem 1 solution: {} [{}s]", ans1, duration.as_secs_f64());

    let start = Instant::now();
    let ans2 = problem2(get_input());
    let duration = start.elapsed();
    println!("Problem 2 solution: {} [{}s]", ans2, duration.as_secs_f64());
}

// PARSING

#[derive(Debug)]
struct AlmanacMapping {
    source_range: Range<i64>,
    dest_range: Range<i64>,
}

impl From<&str> for AlmanacMapping {
    fn from(s: &str) -> Self {
        let s_nums: Vec<_> = s.split_whitespace().take(3).collect();
        let length: i64 = s_nums[2].parse().unwrap();
        let source_start: i64 = s_nums[1].parse().unwrap();
        let dest_start: i64 = s_nums[0].parse().unwrap();
        AlmanacMapping {
            source_range: source_start..source_start + length,
            dest_range: dest_start..dest_start + length,
        }
    }
}

impl AlmanacMapping {
    fn lookup(&self, key: i64) -> Option<i64> {
        if self.source_range.contains(&key) {
            Some(self.dest_range.start + (key - self.source_range.start))
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct AlmanacMappingTable {
    name: String,
    mappings: Vec<AlmanacMapping>,
}

fn range_overlap<T: num::Num + Ord + Copy>(r1: &Range<T>, r2: &Range<T>) -> Option<Range<T>> {
    let new_start = std::cmp::max(r1.start, r2.start);
    let new_end = std::cmp::min(r1.end - T::one(), r2.end - T::one());

    if new_start <= std::cmp::min(r1.end - T::one(), r2.end - T::one())
        && new_end >= std::cmp::max(r1.start, r2.start)
    {
        Some(new_start..new_end + T::one())
    } else {
        None
    }
}

fn range_exclude<T: num::Num + Ord + Copy>(keep: &Range<T>, exclude: &Range<T>) -> Vec<Range<T>> {
    let mut residual = Vec::new();
    if let Some(overlap) = range_overlap(keep, exclude) {
        if keep.start < overlap.start {
            residual.push(keep.start..overlap.start);
        }
        if keep.end > overlap.end {
            residual.push(overlap.end..keep.end);
        }
    } else {
        residual.push(keep.clone());
    }
    residual
}

impl AlmanacMappingTable {
    fn lookup(&self, key: i64) -> i64 {
        self.mappings
            .iter()
            .find_map(|map| map.lookup(key))
            .unwrap_or(key)
    }
    fn lookup_ranges(&self, ranges: Vec<Range<i64>>) -> Vec<Range<i64>> {
        ranges
            .iter()
            .flat_map(|range| self.lookup_range(range))
            .collect()
    }
    fn lookup_range(&self, range: &Range<i64>) -> Vec<Range<i64>> {
        let mut residual = vec![range.clone()];
        let mut resolved_mappings = Vec::new();

        for map in &self.mappings {
            let mut local_residual = Vec::new();
            for range in residual.into_iter() {
                if let Some(overlap) = range_overlap(&range, &map.source_range) {
                    let dest_start_idx = overlap.start - map.source_range.start;
                    let length = overlap.end.saturating_sub(overlap.start);
                    resolved_mappings.push(
                        (map.dest_range.start + dest_start_idx)
                            ..(map.dest_range.start + dest_start_idx + length),
                    );
                    local_residual.extend_from_slice(&range_exclude(&range, &overlap));
                } else {
                    local_residual.push(range);
                }
            }
            residual = local_residual;
        }

        // unresolved maps map 1:1
        resolved_mappings.extend_from_slice(&residual);

        resolved_mappings
    }
}

impl<T: BufRead> From<&mut Lines<T>> for AlmanacMappingTable {
    fn from(lines: &mut Lines<T>) -> Self {
        let name = lines
            .next()
            .unwrap()
            .unwrap()
            .split_once(':')
            .unwrap()
            .0
            .into();
        let mut mappings = Vec::new();
        for line in lines.map(|l| l.unwrap()) {
            if line == "" {
                break;
            }
            mappings.push(AlmanacMapping::from(line.as_str()));
        }
        AlmanacMappingTable { name, mappings }
    }
}

#[derive(Debug)]
struct Almanac {
    seeds: Vec<i64>,
    seed_to_soil: AlmanacMappingTable,
    soil_to_fertilizer: AlmanacMappingTable,
    fertilizer_to_water: AlmanacMappingTable,
    water_to_light: AlmanacMappingTable,
    light_to_temperature: AlmanacMappingTable,
    temperature_to_humidity: AlmanacMappingTable,
    humidity_to_location: AlmanacMappingTable,
}

impl<T: BufRead> From<Lines<T>> for Almanac {
    fn from(mut lines: Lines<T>) -> Self {
        let seeds_s = lines.next().unwrap().unwrap();
        let seeds: Vec<i64> = seeds_s
            .split_once(": ")
            .unwrap()
            .1
            .split_whitespace()
            .map(|s| s.parse().unwrap())
            .collect();
        lines.next();

        Almanac {
            seeds,
            seed_to_soil: AlmanacMappingTable::from(&mut lines),
            soil_to_fertilizer: AlmanacMappingTable::from(&mut lines),
            fertilizer_to_water: AlmanacMappingTable::from(&mut lines),
            water_to_light: AlmanacMappingTable::from(&mut lines),
            light_to_temperature: AlmanacMappingTable::from(&mut lines),
            temperature_to_humidity: AlmanacMappingTable::from(&mut lines),
            humidity_to_location: AlmanacMappingTable::from(&mut lines),
        }
    }
}

impl Almanac {
    fn lookup(&self, seed_id: i64) -> i64 {
        self.humidity_to_location.lookup(
            self.temperature_to_humidity.lookup(
                self.light_to_temperature.lookup(
                    self.water_to_light.lookup(
                        self.fertilizer_to_water.lookup(
                            self.soil_to_fertilizer
                                .lookup(self.seed_to_soil.lookup(seed_id)),
                        ),
                    ),
                ),
            ),
        )
    }
    fn lookup_range(&self, range: Range<i64>) -> Vec<Range<i64>> {
        self.humidity_to_location.lookup_ranges(
            self.temperature_to_humidity.lookup_ranges(
                self.light_to_temperature.lookup_ranges(
                    self.water_to_light.lookup_ranges(
                        self.fertilizer_to_water.lookup_ranges(
                            self.soil_to_fertilizer
                                .lookup_ranges(self.seed_to_soil.lookup_ranges(vec![range])),
                        ),
                    ),
                ),
            ),
        )
    }
}

// PROBLEM 1 solution

// The gardener and his team want to get started as soon as possible, so they'd like to
// know the closest location that needs a seed. Using these maps, find the lowest
// location number that corresponds to any of the initial seeds. To do this, you'll need
// to convert each seed number through other categories until you can find its
// corresponding location number. In this example, the corresponding types are:

//     Seed 79, soil 81, fertilizer 81, water 81, light 74, temperature 78, humidity 78, location 82.
//     Seed 14, soil 14, fertilizer 53, water 49, light 42, temperature 42, humidity 43, location 43.
//     Seed 55, soil 57, fertilizer 57, water 53, light 46, temperature 82, humidity 82, location 86.
//     Seed 13, soil 13, fertilizer 52, water 41, light 34, temperature 34, humidity 35, location 35.

// So, the lowest location number in this example is 35.

// What is the lowest location number that corresponds to any of the initial seed numbers?

fn problem1_lowest_location(almanac: &Almanac) -> i64 {
    almanac
        .seeds
        .iter()
        .map(|seed| almanac.lookup(*seed))
        .min()
        .unwrap()
}

fn problem1<T: BufRead>(input: Lines<T>) -> i64 {
    let almanac = Almanac::from(input);
    problem1_lowest_location(&almanac)
}

// PROBLEM 2 solution

// Everyone will starve if you only plant such a small number of seeds. Re-reading the
// almanac, it looks like the seeds: line actually describes ranges of seed numbers.

// The values on the initial seeds: line come in pairs. Within each pair, the first
// value is the start of the range and the second value is the length of the range. So,
// in the first line of the example above:

// seeds: 79 14 55 13

// This line describes two ranges of seed numbers to be planted in the garden. The first
// range starts with seed number 79 and contains 14 values: 79, 80, ..., 91, 92. The
// second range starts with seed number 55 and contains 13 values: 55, 56, ..., 66, 67.

// Now, rather than considering four seed numbers, you need to consider a total of 27
// seed numbers.

// In the above example, the lowest location number can be obtained from seed number 82,
// which corresponds to soil 84, fertilizer 84, water 84, light 77, temperature 45,
// humidity 46, and location 46. So, the lowest location number is 46.

// Consider all of the initial seed numbers listed in the ranges on the first line of
// the almanac. What is the lowest location number that corresponds to any of the
// initial seed numbers?

fn problem2_lowest_location(almanac: &Almanac) -> i64 {
    let seed_ranges = almanac
        .seeds
        .iter()
        .tuples()
        .map(|(range_start, length)| *range_start..*range_start + *length);

    // seed_ranges
    //     .map(|range| {
    //         range
    //             .into_par_iter()
    //             .map(|x| almanac.lookup(x))
    //             .min()
    //             .unwrap()
    //     })
    //     .min()
    //     .unwrap()
    let ranges = seed_ranges
        .flat_map(|range| almanac.lookup_range(range))
        .collect_vec();

    ranges.iter().map(|range| range.start).min().unwrap()
}

fn problem2<T: BufRead>(input: Lines<T>) -> i64 {
    let almanac = Almanac::from(input);
    problem2_lowest_location(&almanac)
}

#[cfg(test)]
mod tests {
    use num::Num;
    use std::io::{BufRead, Cursor};
    use test_case::test_case;

    use crate::*;

    const EXAMPLE_ALMANAC: &str = &"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn test_almanac_mapping_parse() {
        let s = "50 98 2";
        let a = AlmanacMapping::from(s);
        assert_eq!(a.source_range, 98..100);
        assert_eq!(a.dest_range, 50..52);
    }

    #[test]
    fn test_almanac_mapping_lookup() {
        let s = "50 98 2";
        let a = AlmanacMapping::from(s);
        assert_eq!(a.lookup(99), Some(51));
    }

    #[test]
    fn test_almanac_parsing() {
        let c = Cursor::new(EXAMPLE_ALMANAC);
        let a = Almanac::from(c.lines());
        assert_eq!(a.lookup(79), 82);
    }

    #[test_case(0..5, 2..4, Some(2..4) ; "one range overlaps completely")]
    #[test_case(0..5, 2..10, Some(2..5) ; "one range overlaps from below")]
    #[test_case(0..5, 5..10, None ; "no overlap, touching")]
    #[test_case(0..5, 6..10, None ; "no overlap, disjoint")]
    fn test_range_overlap<T: Num + Copy + Ord + std::fmt::Debug>(
        r1: Range<T>,
        r2: Range<T>,
        expected: Option<Range<T>>,
    ) {
        assert_eq!(range_overlap(&r1, &r2), expected);
    }

    #[test]
    fn test_problem1_example() {
        let c = Cursor::new(EXAMPLE_ALMANAC);
        let a = Almanac::from(c.lines());
        assert_eq!(problem1_lowest_location(&a), 35);
    }

    #[test]
    fn test_problem2_example() {
        let c = Cursor::new(EXAMPLE_ALMANAC);
        let a = Almanac::from(c.lines());
        assert_eq!(problem2_lowest_location(&a), 46);
    }
}
