use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::ops::Index;
use std::time::Instant;

// BOILERPLATE
type InputIter = Lines<BufReader<File>>;

fn get_input() -> InputIter {
    let f = File::open("input").unwrap();
    let br = BufReader::new(f);
    br.lines()
}

fn main() {
    let start = Instant::now();
    let ans1 = problem1(get_input());
    let duration = start.elapsed();
    println!("Problem 1 solution: {} [{}s]", ans1, duration.as_secs_f64());

    let start = Instant::now();
    let ans2 = problem2(get_input());
    let duration = start.elapsed();
    println!("Problem 2 solution: {} [{}s]", ans2, duration.as_secs_f64());
}

// PARSE

#[derive(Debug)]
struct Pattern {
    mirrors: Vec<Vec<char>>,
}

impl Pattern {
    fn width(&self) -> usize {
        self.mirrors[0].len()
    }
    fn height(&self) -> usize {
        self.mirrors.len()
    }
    fn col(&self, idx: usize) -> Vec<char> {
        self.mirrors.iter().map(|row| row[idx]).collect()
    }

    fn find_horizontal_reflection(&self) -> Option<usize> {
        (1..self.height()).find(|idx| self.is_horizontal_reflection(*idx))
    }

    fn is_horizontal_reflection(&self, idx: usize) -> bool {
        let row_pairs = std::cmp::min(idx, self.height() - idx);
        (0..row_pairs).all(|offset| self.mirrors[idx - offset - 1] == self.mirrors[idx + offset])
    }

    fn find_vertical_reflection(&self) -> Option<usize> {
        (1..self.width()).find(|idx| self.is_vertical_reflection(*idx))
    }

    fn is_vertical_reflection(&self, idx: usize) -> bool {
        let col_pairs = std::cmp::min(idx, self.width() - idx);
        (0..col_pairs).all(|offset| self.col(idx - offset - 1) == self.col(idx + offset))
    }

    fn find_smudged_horizontal_reflection(&self) -> Option<usize> {
        (1..self.height()).find(|idx| self.is_smudged_horizontal_reflection(*idx))
    }

    fn is_smudged_horizontal_reflection(&self, idx: usize) -> bool {
        // Same algorithm for problem 2, but count errors breaking reflection.
        // If the count == 1 after all checks, then it is our smudge
        let row_pairs = std::cmp::min(idx, self.height() - idx);
        (0..row_pairs)
            .map(|offset| {
                self.mirrors[idx - offset - 1]
                    .iter()
                    .zip(self.mirrors[idx + offset].iter())
                    .filter(|(a, b)| **a != **b)
                    .count()
            })
            .sum::<usize>()
            == 1
    }

    fn find_smudged_vertical_reflection(&self) -> Option<usize> {
        (1..self.width()).find(|idx| self.is_smudged_vertical_reflection(*idx))
    }

    fn is_smudged_vertical_reflection(&self, idx: usize) -> bool {
        let col_pairs = std::cmp::min(idx, self.width() - idx);
        (0..col_pairs)
            .map(|offset| {
                self.col(idx - offset - 1)
                    .iter()
                    .zip(self.col(idx + offset).iter())
                    .filter(|(a, b)| **a != **b)
                    .count()
            })
            .sum::<usize>()
            == 1
    }

    fn reflection_cost(&self) -> u64 {
        if let Some(refl) = self.find_horizontal_reflection() {
            100 * refl as u64
        } else if let Some(refl) = self.find_vertical_reflection() {
            refl as u64
        } else {
            panic!("no reflection");
        }
    }

    fn smudged_reflection_cost(&self) -> u64 {
        if let Some(refl) = self.find_smudged_horizontal_reflection() {
            100 * refl as u64
        } else if let Some(refl) = self.find_smudged_vertical_reflection() {
            refl as u64
        } else {
            panic!("no reflection");
        }
    }
}

#[derive(Debug)]
struct Patterns {
    patterns: Vec<Pattern>,
}

impl<T: BufRead> From<Lines<T>> for Patterns {
    fn from(mut lines: Lines<T>) -> Self {
        let mut patterns = Vec::new();
        let mut cur_lines = Vec::new();
        while let Some(Ok(line)) = lines.next() {
            if line.len() == 0 {
                patterns.push(Pattern { mirrors: cur_lines });
                cur_lines = Vec::new();
            } else {
                cur_lines.push(line.chars().collect());
            }
        }
        patterns.push(Pattern { mirrors: cur_lines });
        Self { patterns }
    }
}

// PROBLEM 1 solution

fn problem1<T: BufRead>(input: Lines<T>) -> u64 {
    let patterns = Patterns::from(input);

    patterns
        .patterns
        .iter()
        .map(|pat| pat.reflection_cost())
        .sum()
}

// PROBLEM 2 solution
fn problem2<T: BufRead>(input: Lines<T>) -> u64 {
    let patterns = Patterns::from(input);

    patterns
        .patterns
        .iter()
        .map(|pat| pat.smudged_reflection_cost())
        .sum()
}

#[cfg(test)]
mod tests {
    use crate::*;
    use std::io::Cursor;

    const EXAMPLE: &str = &"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

    #[test]
    fn problem1_find_horizontal() {
        let c = Cursor::new(EXAMPLE);
        let patterns = Patterns::from(c.lines());
        assert_eq!(patterns.patterns[1].find_horizontal_reflection(), Some(4));
    }

    #[test]
    fn problem1_find_vertical() {
        let c = Cursor::new(EXAMPLE);
        let patterns = Patterns::from(c.lines());
        assert_eq!(patterns.patterns[0].find_vertical_reflection(), Some(5));
    }

    #[test]
    fn problem1_example() {
        let c = Cursor::new(EXAMPLE);
        assert_eq!(problem1(c.lines()), 405);
    }

    #[test]
    fn problem2_find_horizontal() {
        let c = Cursor::new(EXAMPLE);
        let patterns = Patterns::from(c.lines());
        assert_eq!(
            patterns.patterns[0].find_smudged_horizontal_reflection(),
            Some(3)
        );
        assert_eq!(
            patterns.patterns[1].find_smudged_horizontal_reflection(),
            Some(1)
        );
    }

    #[test]
    fn problem2_example() {
        let c = Cursor::new(EXAMPLE);
        assert_eq!(problem2(c.lines()), 400);
    }
}
